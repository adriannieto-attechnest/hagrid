#!/bin/bash
set -e

echo "Starting sendmail"
echo "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
service sendmail restart

[[ -z "$ROCKET_SECRET_KEY" ]] && export ROCKET_SECRET_KEY=$(openssl rand -base64 32)
[[ -z "$TOKEN_SECRET" ]] && export TOKEN_SECRET=$(openssl rand -hex 32)

echo "Creating config file"

cat <<EOF > Rocket.toml
[global]
address = "0.0.0.0"
port = $HTTP_PORT

[production]
base-URI = "$BASE_URI"
base-URI-Onion = "$BASE_URI"
from = "$EMAIL_FROM"
x-accel-redirect = true
token_secret = "$TOKEN_SECRET"
token_validity = $TOKEN_VALIDITY_SECONDS
template_dir = "templates"
keys_internal_dir = "$STORAGE_PATH/internal/keys"
keys_external_dir = "$STORAGE_PATH/external/keys"
assets_dir = "assets"
token_dir = "tokens"
tmp_dir = "$STORAGE_PATH/tmp"
mail_rate_limit = $EMAIL_RATE_LIMIT
maintenance_file = "maintenance"
enable_prometheus = false
email_template_dir = "email-templates"
EOF

echo "Starting Hagrid Keyserver"
./hagrid
