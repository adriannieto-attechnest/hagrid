FROM debian:bullseye-slim as build

WORKDIR /root

# Install packages
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    ca-certificates curl file \
    build-essential \
    autoconf automake autotools-dev libtool xutils-dev \
    gnutls-bin nettle-dev gcc llvm-dev libclang-dev pkg-config gettext \
    zsh && \
    rm -rf /var/lib/apt/lists/*

# Install toolchain
RUN curl https://sh.rustup.rs -sSf | \
    sh -s -- --default-toolchain stable -y
ENV PATH=/root/.cargo/bin:$PATH

# Cache dependencies
COPY rust-toolchain Cargo.lock Cargo.toml ./
COPY database/Cargo.toml database/Cargo.toml 
RUN mkdir -p hagridctl/src && touch hagridctl/src/lib.rs 
COPY hagridctl/Cargo.toml hagridctl/Cargo.toml 
RUN mkdir -p database/src && touch database/src/lib.rs 
RUN mkdir -p .cargo
RUN cargo vendor > .cargo/config
RUN rm -rf **/*.rs

# Copy source
COPY . . 

# Make translated templates
RUN  zsh ./make-translated-templates

# Build and test
RUN RUST_BACKTRACE=full cargo build --release
RUN RUST_BACKTRACE=full cargo test --release --all

FROM debian:bullseye-slim as dist

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    gnutls-bin nettle-dev gcc llvm-dev libclang-dev pkg-config gettext \
    openssl sendmail curl

WORKDIR /app
COPY docker-entrypoint.sh .
COPY --from=build /root/target/release/hagrid .
COPY --from=build /root/dist/ .
RUN chmod +x docker-entrypoint.sh hagrid

ENV BASE_URI="https://keys.openpgp.org" \
    HTTP_PORT=8000 \
    EMAIL_FROM="noreply@keys.openpgp.org" \
    EMAIL_RATE_LIMIT=60 \
    ROCKET_SECRET_KEY="" \
    TOKEN_SECRET="" \
    TOKEN_VALIDITY_SECONDS=3600 \
    STORAGE_PATH="storage"

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl --fail http://localhost:$HTTP_PORT || exit 1" ]
ENTRYPOINT [ "./docker-entrypoint.sh" ]